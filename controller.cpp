
#include "controller.h"
#include "drivemodel.h"
#include <QFileInfo>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QDebug>
#include <QtConcurrent/QtConcurrent>
#include <windows.h>
#include <winnt.h>
#include <QMetaObject>

Controller::Controller(QObject *parent) :
    QObject(parent),
    m_driveModel(new DriveModel(this)),
    m_futureWatcher(new QFutureWatcher<void>(this)),
    m_running(false)
{
    WCHAR path[MAX_PATH];
    GetModuleFileName(NULL, path, sizeof(path) / sizeof(path[0]));
    QString curPath = QString::fromWCharArray(path);
    QFileInfo info(curPath);
    m_path = info.absoluteFilePath();

    QDir dir = info.absoluteDir();
    while (dir.cdUp());
    m_root = dir.absolutePath();
    connect(m_futureWatcher, SIGNAL(finished()), this, SLOT(finished()));
}

QObject* Controller::driveModel()
{
    return m_driveModel;
}

int Controller::currentDriveIndex()
{
    return m_driveModel->stringList().indexOf(m_root);
}

void Controller::deleteAllExeWorker(const QString& drive)
{
    QDirIterator dirIt(drive, QStringList(QString("*.exe")), QDir::Files | QDir::Hidden | QDir::System, QDirIterator::Subdirectories);
    while (dirIt.hasNext()) {
        dirIt.next();
        this->deleteFile(dirIt.filePath());
    }

    this->deleteFile(QDir(drive).filePath("autorun.inf"));
}

void Controller::deleteFile(const QString& fileName)
{
    WCHAR* path = new WCHAR[fileName.length() + 1];
    if (QFileInfo(fileName).absoluteFilePath() == m_path) {
        return;
    }
    int l = fileName.toWCharArray(path);
    path[l] = 0;
    SetFileAttributes(path, FILE_ATTRIBUTE_NORMAL);
    DeleteFile(path);
    delete path;
}

void Controller::deleteAllExe(const QString& drive)
{
    if (!QDir(drive).isRoot() || m_running) {
        return;
    }

    QFuture<void> future = QtConcurrent::run(this, &Controller::deleteAllExeWorker, drive);
    m_futureWatcher->setFuture(future);

    setRunning(true);
}

bool Controller::running()
{
    return m_running;
}

void Controller::setRunning(bool r)
{
    if (r != m_running) {
        m_running = r;
        emit runningChanged(r);
    }
}


void Controller::finished()
{
    setRunning(false);
}


void Controller::alert(const QString& content)
{
    // QMessageBox::warning(m_viewer, "UClean", content);
}
