CONFIG += static c++11
DEFINES += STATIC
QT += declarative

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

LFLAGS = -static

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    controller.cpp \
    drivemodel.cpp

# Installation path
# target.path =

RC_FILE += logo.rc

HEADERS += \
    controller.h \
    drivemodel.h

RESOURCES += \
    uclean.qrc

OTHER_FILES += \
    logo.rc \
    qml/uclean/CloseButton.qml \
    qml/uclean/DriveList.qml \
    qml/uclean/main.qml \
    qml/uclean/StartButton.qml \
    qml/uclean/MsgDialog.qml

