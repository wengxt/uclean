#include <QtGui>
#include <QtWidgets>
#include <QtDeclarative>
#include "controller.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QDeclarativeView viewer;
    viewer.setAttribute(Qt::WA_NoSystemBackground);
    viewer.viewport()->setAttribute(Qt::WA_TranslucentBackground);
    viewer.viewport()->setAttribute(Qt::WA_NoSystemBackground);
    viewer.setWindowFlags(Qt::FramelessWindowHint);
    viewer.setBackgroundBrush(QBrush(Qt::transparent));
    viewer.setAttribute(Qt::WA_TranslucentBackground);
    QObject::connect(viewer.engine(), &QDeclarativeEngine::quit, &app, QApplication::quit);

    auto context = viewer.rootContext();
    context->setContextProperty("viewerWidget", &viewer);
    Controller c(&viewer);
    context->setContextProperty("controller", &c);

    // load qml file at least to avoid undefined context property
    viewer.setSource(QUrl("qrc:///qml/uclean/main.qml"));
    viewer.show();
    return app.exec();
}
