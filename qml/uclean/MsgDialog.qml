import QtQuick 1.0

Item {
    id: dialog
    property bool warning: false
    property alias text: label.text
    property bool quit: false

    width: background.sourceSize.width
    height: background.sourceSize.height

    Image {
        id: background
        source: "img/msgbox.png"
        width: background.sourceSize.width
        height: background.sourceSize.height

        Image {
            x: (parent.width - width) / 2
            y: 43
            width: sourceSize.width
            height: sourceSize.height
            source: warning ? "img/warning.png" : "img/ok.png"
        }

        Text {
            id: label
            x: (parent.width - width) / 2
            y: 120
            width: paintedWidth
            height: paintedHeight
            font.family: "Microsoft YaHei"
            font.pixelSize: 12
        }



        Image {
            x: (parent.width - width) / 2
            y: 150
            source: mouseArea.containsMouse ? (mouseArea.pressed ? "img/okb_focus.png" : "img/okb_hover.png") : "img/okb.png"

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (dialog.quit) {
                        Qt.quit()
                    }
                    dialog.visible = false;
                    dialog.callback = null;
                }
            }
        }
    }

}
