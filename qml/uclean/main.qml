import QtQuick 1.0

Item {
    height: 335
    width: 560
    BorderImage {
        id: background
        source: "img/background.png"
        width: 560; height: 335
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5
    }
    MouseArea {
        anchors.fill: parent
        property variant previousPosition
        onPressed: {
            previousPosition = Qt.point(mouseX, mouseY)
        }
        onPositionChanged: {
            if (pressedButtons == Qt.LeftButton) {
                var dx = mouseX - previousPosition.x
                var dy = mouseY - previousPosition.y
                viewerWidget.pos = Qt.point(viewerWidget.x + dx,
                                            viewerWidget.y + dy);
            }
        }
    }

    Component.onCompleted : {
        driveList.index = controller.currentDriveIndex();
        controller.runningChanged.connect(function () {
            qmlAlert(false, "清理完成", true)
        })
    }


    DriveList {
        id: driveList
        height: 40
        width: 200
        anchors {
            bottom: parent.bottom
            bottomMargin: 33
            left: parent.left
            leftMargin: 40
        }

        z: 1

        model: controller.driveModel
    }

    CloseButton {
        anchors {
            top: parent.top
            topMargin: 10
            right: parent.right
            rightMargin: 28
        }
    }

    StartButton {
        running: controller.running

        anchors {
            bottom: parent.bottom
            bottomMargin: 33
            right: parent.right
            rightMargin: 40
        }

        onClicked: {
            if (driveList.drive.length == 0) {
                if (driveList.count == 0) {
                    qmlAlert(true, "您没有使用U盘", false)
                } else {
                    qmlAlert(true, "请选择要清除的分区", false)
                }
            } else {
                controller.deleteAllExe(driveList.drive)
            }
        }
    }

    function qmlAlert(warning, text, quit) {
        dialog.warning = warning
        dialog.quit = quit
        dialog.text = text
        dialog.visible = true
    }

    Rectangle {
        visible: dialog.visible
        anchors {
            fill: parent
            leftMargin: 15
            rightMargin: 15
            topMargin: 9
            bottomMargin: 9
        }

        color: "black"
        opacity: 0.5
        z: 2

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
        }
    }

    MsgDialog {
        visible: false
        id: dialog
        z: 3
        anchors.centerIn: parent
    }
}
