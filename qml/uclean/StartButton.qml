import QtQuick 1.0

Rectangle {
    id: button
    signal clicked
    property bool running : false

    height: 42
    width: 146
    AnimatedImage {
        id: button1
        source: "img/start_01.gif"
        width: 146; height: 42
        opacity: running ? 0 : (mouseArea2.containsMouse ? 0 : 1)
    }
    AnimatedImage {
        id: button2
        source: "img/str.gif"
        width: 146; height: 42
        opacity: running ? 0 : (!mouseArea2.containsMouse ? 0 : 1)
    }
    AnimatedImage {
        id: button3
        source: "img/ing.gif"
        width: 146; height: 42
        opacity: running ? 1 : 0
    }
    MouseArea {
        id: mouseArea2
        anchors.fill: parent
        hoverEnabled: true
        onClicked: button.clicked()
    }

}
