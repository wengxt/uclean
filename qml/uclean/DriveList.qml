import QtQuick 1.0

Item {
    property alias model: view.model
    property alias index: view.currentIndex
    property string drive: view.currentItem ? view.currentItem.drive : ""
    property alias count: view.count

    ListView {
        id: view
        anchors.fill: parent
        Component {
            id: driveDelegate
            Rectangle {
                property string drive: model.display
                color: "#ececec"
                width: 60
                height: 40
                radius: 10
                Text {
                    anchors.centerIn: parent
                    text: model.display
                }

                MouseArea {
                    acceptedButtons: Qt.LeftButton
                    anchors.fill: parent
                    onClicked : {
                        view.currentIndex = model.index
                    }
                }
            }
        }

        Component {
            id: highlight
            Rectangle {
                width: 60
                height: 40
                color: "#25beff"
                radius: 10
                opacity: 0.3
                z: 1
            }
        }

        orientation: Qt.Horizontal

        delegate: driveDelegate
        highlight: highlight
        highlightFollowsCurrentItem: true
    }

}
