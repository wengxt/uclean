import QtQuick 1.0


Rectangle {
    height: 20
    width: 38
    BorderImage {
        id: close1
        source: "img/close.png"
        width: 38; height: 20
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5
        opacity:mouseArea1.containsMouse ? 0 : 1
    }
    BorderImage {
        id: close2
        source: "img/close_hover.png"
        width: 38; height: 20
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5
        opacity: mouseArea1.containsMouse ? 1 : 0
    }

    MouseArea {
       id: mouseArea1
       anchors.fill: parent
       hoverEnabled: true
       onClicked: {
           Qt.quit();
       }
    }
}
