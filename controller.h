#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QFutureWatcher>

class DriveModel;
class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* driveModel READ driveModel)
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
public:
    explicit Controller(QObject *parent = 0);

    QObject *driveModel();
    Q_INVOKABLE int currentDriveIndex();
    Q_INVOKABLE void alert(const QString& content);
    Q_INVOKABLE void deleteAllExe(const QString& drive);
    bool running();

signals:
    void runningChanged(bool running);

private slots:
    void finished();

private:
    void deleteAllExeWorker(const QString& drive);
    void deleteFile(const QString& path);
    void setRunning(bool running);

private:
    DriveModel* m_driveModel;
    QFutureWatcher<void>* m_futureWatcher;
    bool m_running;
    QString m_path;
    QString m_root;
};

#endif // CONTROLLER_H
