#include "drivemodel.h"
#include <QDir>
#include <QDebug>
#include <windows.h>

static BOOL GetDisksProperty(HANDLE hDevice,
                             PSTORAGE_DEVICE_DESCRIPTOR pDevDesc)
{
     STORAGE_PROPERTY_QUERY Query; // input param for query
     DWORD dwOutBytes; // IOCTL output length
     BOOL bResult; // IOCTL return val

     // specify the query type
     Query.PropertyId = StorageDeviceProperty;
     Query.QueryType = PropertyStandardQuery;

     // Query using IOCTL_STORAGE_QUERY_PROPERTY
     bResult = ::DeviceIoControl(hDevice, // device handle
                                 IOCTL_STORAGE_QUERY_PROPERTY, // info of device property
                                 &Query, sizeof(Query), // input data buffer
                                 pDevDesc, sizeof(*pDevDesc), // output data buffer
                                 &dwOutBytes, // out's length
                                 (LPOVERLAPPED)NULL);

     return bResult;
}

DriveModel::DriveModel(QObject *parent) :
    QStringListModel(parent)
{
    QStringList l;
    foreach(const QFileInfo& info, QDir::drives()) {
        QString qtPath = info.absoluteFilePath();
        int idx;
        if ((idx = qtPath.indexOf('/')) >= 0) {
            qtPath[idx] = '\\';
        }
        WCHAR path[MAX_PATH];
        int len = qtPath.toWCharArray(path);
        path[len] = 0;

        auto driveType = ::GetDriveType(path);
        if (driveType == DRIVE_REMOVABLE || driveType == DRIVE_FIXED) {
            WCHAR unipath[MAX_PATH];
            qtPath = qtPath.prepend("\\\\?\\");
            qtPath.chop(1);
            int len = qtPath.toWCharArray(unipath);
            unipath[len] = 0;
            auto hDevice = ::CreateFile(unipath,
                                        0,
                                        FILE_SHARE_READ | FILE_SHARE_WRITE,
                                        0, OPEN_EXISTING, 0, NULL);

            if (hDevice != INVALID_HANDLE_VALUE) {
                STORAGE_DEVICE_DESCRIPTOR devDesc;
                if(::GetDisksProperty(hDevice, &devDesc))
                {
                    if(devDesc.BusType == BusTypeUsb) {
                       l << info.absoluteFilePath();
                    }
                }
                CloseHandle(hDevice);
            }
        }
    }

    setStringList(l);
}
